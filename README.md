# DecisionTreeClassifier overfitting prevention demonstration

This repository contains a Python script that demonstrates how playing with the `max_depth` parameter of a `DecisionTreeClassifier` can improve prediction results by avoiding overfitting, using the breast cancer dataset from `sklearn.datasets`.

## Overview

The script performs the following steps:
1. Loads the breast cancer dataset.
2. Splits the data into training and testing sets.
3. Trains a `DecisionTreeClassifier` using default parameters.
4. Evaluates and prints the accuracy on both the training and testing sets.
5. Trains a `DecisionTreeClassifier` with `max_depth=4` to reduce overfitting.
6. Evaluates and prints the accuracy on both the training and testing sets again.

## Results

By comparing the accuracy of the model on the training and testing datasets with and without setting `max_depth`, we can observe the effect of overfitting:
- The default `DecisionTreeClassifier` with no specified `max_depth` tends to overfit the training data, resulting in high training accuracy but lower test accuracy.
- Specifying `max_depth=4` results in better generalization, with lower discrepancy between training and testing accuracy.

## How to Run the Script

1. Ensure you have Python installed.
2. Install the necessary libraries:
    ```bash
    pip install scikit-learn
    ```
3. Run the script:
    ```bash
    python decision_tree.py
    ```

## Example Output

```plaintext
Running prediction with default parameters - DecisionTreeClassifier(random_state=0)
Accuracy on training set: 1.000
Accuracy on test set: 0.937


Running prediction with max_depth=4 to prevent overfitting - DecisionTreeClassifier(random_state=0, max_depth=4)
Accuracy on training set: 0.988
Accuracy on test set: 0.951
